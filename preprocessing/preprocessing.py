import pandas
from PIL import Image

filename = '../mnist_train.csv'
trainDataset = pandas.read_csv(filename,skipinitialspace=True)

filename = '../mnist_test.csv'
testDataset = pandas.read_csv(filename,skipinitialspace=True)

array = trainDataset.values
XTrain = array[:,1:785]
YTrain = array[:,0]

array = testDataset.values
XTest = array[:,1:785]
YTest = array[:,0]
print(XTest.shape)

im = Image.new("RGB", (28, 28))
pix = im.load()
i=0
for x in range(28):
    for y in range(28):
        pix[x,y] = (XTest[2,i],XTest[2,i],XTest[2,i])
        i+=1
im.save("test.png", "PNG")